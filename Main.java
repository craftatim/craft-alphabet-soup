package org.craft;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {

    public static void main(String[] args) {
        String INPUT_FILE = "/Users/tim.craft/IdeaProjects/craft-alphabet-soup/input.txt";
        try {

            BufferedReader br = new BufferedReader(new FileReader(INPUT_FILE));

            List<List<String>> puzzle = fillPuzzle(br);

            List<String[]> words = getWords(br);

            findWords(puzzle, words);

        } catch(FileNotFoundException e){
            System.out.println("Sorry, no input provided");
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("Runtime Exception");
            throw new RuntimeException(e);
        }
    }

    private static List<List<String>> fillPuzzle(BufferedReader reader) throws IOException {

        List<List<String>> puzzle = new ArrayList<>();

        String dimensions = reader.readLine();
        String[] width_height = dimensions.split("x");

        int height = Integer.parseInt(width_height[0]);
        int width = Integer.parseInt(width_height[1]);

        for(int i = 0; i < height; i++){
            String next_line = reader.readLine();
            List<String> row = new ArrayList<>(width);
            String[] split_row = next_line.split(" ");
            row = Arrays.stream(split_row)
                    .collect(Collectors.toList());
            puzzle.add(row);
        }
        return puzzle;
    }

    //grab all lines below the grid and covert each word to list of characters. keeping it as list of characters
    //makes it easier to iterate over each letter on searches.
    private static List<String[]> getWords(BufferedReader reader) throws IOException {
        String word;
        List<String[]> words = new ArrayList<>();
        do {
            word = reader.readLine();
            if(word != null){
                words.add(word.split("(?!^)"));
            }
        } while(word != null);
        return words;
    }

    private static void findWords(List<List<String>> puzzle, List<String[]> words){
        for(int i = 0; i < words.size(); i++){
            String[] word_chars = words.get(i);
            int[] starting_coordinate = new int[2];
            for(int j = 0; j < puzzle.size(); j++){
                boolean found_starting_point = false;
                if(puzzle.get(j).contains(word_chars[0])){
                    //set starting point of a word
                    starting_coordinate[0] = j;
                    starting_coordinate[1] = puzzle.get(j).indexOf(word_chars[0]);
                    found_starting_point = true;
                }
                if(found_starting_point){
                    int[] final_coordinate = check_next_coordinate(starting_coordinate, puzzle, word_chars);
                    if(check_in_bounds(final_coordinate, puzzle)){
                        System.out.println(String.join("", word_chars) + ": " + Arrays.toString(starting_coordinate) + " " + Arrays.toString(final_coordinate));
                    }
                }
            }
        }
    }

    private static int[] check_next_coordinate(int[] starting_coordinate, List<List<String>> puzzle, String[] word){
        int[] ending_coordinate = new int[2];
        int cursor = 1;

        //Iterate through clockwise around starting coordinate to find the next letter in the word. on a match, call
        //check_letter recursively with the same vector coordinates until the word either is completely found or there
        //is a mismatch.
        if(!check_letter(starting_coordinate, puzzle, word, cursor, -1, 0, ending_coordinate)){
            if (!check_letter(starting_coordinate, puzzle, word, cursor, -1, +1, ending_coordinate)){
                if(!check_letter(starting_coordinate, puzzle, word, cursor, 0, +1, ending_coordinate)){
                    if(!check_letter(starting_coordinate, puzzle, word, cursor, +1, +1, ending_coordinate)){
                        if (!check_letter(starting_coordinate, puzzle, word, cursor, +1, 0, ending_coordinate)){
                            if(!check_letter(starting_coordinate, puzzle, word, cursor, +1, -1, ending_coordinate)){
                                if(!check_letter(starting_coordinate, puzzle, word, cursor, 0, -1, ending_coordinate)){
                                    if(!check_letter(starting_coordinate, puzzle, word, cursor, -1, -1, ending_coordinate)){
                                        ending_coordinate[0] = -1;
                                        ending_coordinate[1] = -1;
                                        /*return coordinates for a non word ending as out of bounds*/
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return ending_coordinate;
    }

    private static boolean check_letter(int[] starting_coordinate, List<List<String>> puzzle, String[] word, int cursor, int row_adj, int col_adj, int[] ending_coordinate){
        boolean correct_course = false;
        int[] coordinate = new int[2];
        int row = 0, column = 1;

        coordinate[row] = starting_coordinate[row] + row_adj;
        coordinate[column] = starting_coordinate[column] + col_adj;

        if(check_in_bounds(coordinate, puzzle)){
            if(word[cursor].equalsIgnoreCase(puzzle.get(coordinate[0]).get(coordinate[1]))){
                if(cursor == word.length - 1){
                    correct_course = true;
                    ending_coordinate[0] = coordinate[0];
                    ending_coordinate[1] = coordinate[1];
                } else {
                    correct_course = check_letter(coordinate, puzzle, word, ++cursor, row_adj, col_adj, ending_coordinate);
                }
            }
        }
        return correct_course;
    }


    private static boolean check_in_bounds(int[] next_coordinate, List<List<String>> puzzle){
        try {
            if(puzzle.get(next_coordinate[0]).get(next_coordinate[1]) != null){
                return true;
            }
        } catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }
}